package org.lftp.misc;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;
import org.lftp.connection.FTPConnection;

/**
 * Utility class for managing server tasks.
 * @author alexandr-osprey
 */
public class MaintanceTools {
    public static final ScheduledExecutorWrapper THREADSUPERVISOR;
    private static int IDLE = 60*10;
    private static final ChronoUnit IDLEUNIT = ChronoUnit.SECONDS;
    static {
        THREADSUPERVISOR = new ScheduledExecutorWrapper(1);
    }
    
    public static void setIdleMin(int min) {
        IDLE = min*60;
    }

    /**
     * Terminates FTP connections which idle for specified period and not data connected.
     */
    public static void launchConnectionsSupervising() {
        THREADSUPERVISOR.scheduleWithFixedDelay(() -> { 
            FTPConnection.closeIdle(IDLEUNIT, IDLE);
        }, IDLE, IDLE, TimeUnit.SECONDS);
    }
}
