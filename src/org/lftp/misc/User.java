package org.lftp.misc;

import java.io.*;
import org.lftp.ioprocessing.IOProcessing;

/**
 * Class, representing a user.
 * @author alexandr-osprey
 */
public class User {
    private final String username;
    private static String PATHTOUSERDATA;
    
    public static User getUser(String username, String password) {
        if (check(username, password)) {
            return new User(username);
        } else {
            return null;
        }
        
    }
    private User(String username) {
       this.username = username;
    }
    @Override
    public String toString() {
        return this.username;
    }
    private static boolean check(String username, String password) {
        String loginInfo;
        BufferedReader userDataReader;
        try {
            userDataReader = new BufferedReader(new FileReader(PATHTOUSERDATA));
            while ((loginInfo = userDataReader.readLine()) != null) {
                String[] st = loginInfo.split(" ");
                if(username.equals(st[0]) && password.equals(st[1]))
                {
                    return true;
                }
            }
        } catch(IOException e) {
            System.err.println(e);
        }
        return false;
    }
    public static void setPathToData(String path) {
        PATHTOUSERDATA = path;
    }
    public static boolean exists(String username) {
        String loginInfo;
        boolean validUsername = false;
        try {
            BufferedReader userDataReader = 
                    new BufferedReader(new FileReader(PATHTOUSERDATA));
            while ((loginInfo = userDataReader.readLine()) != null) {
                String[] st = loginInfo.split(" ");
                if (username.equals(st[0]))
                {
                    validUsername = true;
                    break;
                }
            }
        } catch(IOException e) {
            IOProcessing.writeSystemMessage(e.getMessage());
        }
        return validUsername;
    }   
    public String getUsername() {
        return this.username;
    }
}
