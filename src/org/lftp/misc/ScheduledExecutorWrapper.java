package org.lftp.misc;

import java.util.*;
import java.util.concurrent.*;

/**
 * Wrapper class for suppressing exceptions.
 * @author operator
 */
public class ScheduledExecutorWrapper extends ScheduledThreadPoolExecutor {
    public ScheduledExecutorWrapper(int poolSize) {
        super(poolSize);
    }
    @Override
    public ScheduledFuture scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return super.scheduleWithFixedDelay(wrapRunnable(command), initialDelay, delay, unit);
    }
    private Runnable wrapRunnable(Runnable command) {
        return new LogOnExceptionRunnable(command);
    }
    private class LogOnExceptionRunnable implements Runnable {
        private final Runnable theRunnable;

        public LogOnExceptionRunnable(Runnable theRunnable) {
            super();
            this.theRunnable = theRunnable;
        }

        @Override
        public void run() {
            try {
                theRunnable.run();
            } catch (ConcurrentModificationException e) {
                // LOG IT HERE!!!
                System.err.println("error in executing: " + theRunnable + ", but it will be run. ");
                e.printStackTrace();
            } catch (Exception e) {
                System.err.println("error in executing: " + theRunnable + ", it won't be run. ");
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }
}