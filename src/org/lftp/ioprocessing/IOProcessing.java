package org.lftp.ioprocessing;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.util.*;
import javafx.scene.control.TextInputControl;

//

/**
 * Noninstaniable static utility class class for input-output operations with files, channels.
 * @author alexandr-osprey
 */
public class IOProcessing {
    public static final String LINESEPARATOR = System.getProperty("line.separator");
    public static final String EMPTY = "";
    private static final List<TextInputControl> UIOUTPUTFIELDS = Collections.synchronizedList(new ArrayList<>());

    // Suppress default constructor for noninstantiability
    private IOProcessing() {
        throw new AssertionError();
    }

    /**
     * Reads bytes from given channel to file output with specified options from given position.
     * @param inputChannel channel to read from.
     * @param output file to write to.
     * @param startPosition byte offset for writing to file.
     * @param options open options for output file.
     * @return sum of read bytes.
     * @throws IOException thrown if error in reading or output occured.
     */
    public static long readBytes(ReadableByteChannel inputChannel, Path output, long startPosition, StandardOpenOption... options) throws IOException  {
        long bytesSum = 0;
        long bytesRead = 0;
        try (FileChannel outputChannel = FileChannel.open(output, options)) {
            do {
                bytesRead = outputChannel.transferFrom(inputChannel, startPosition, Long.MAX_VALUE);
                bytesSum += bytesRead;
            } while (bytesRead != 0);
        }
        return bytesSum;
    }

    /**
     * Writes byte array to given byte channel.
     * @param channel channel to write to.
     * @param writeBuffer buffer used for writing.
     * @param array byte array to write from.
     * @throws IOException thrown if error in writing occured.
     */
    public static void writeByteArray(WritableByteChannel channel, ByteBuffer writeBuffer, byte[] array) throws IOException  {
        writeBuffer.put(array);
        writeBuffer.flip();
        channel.write(writeBuffer);
        writeBuffer.compact();
    }

    /**
     * Writes bytes from given path to byte channel from starting position with given options.
     * @param source path of source file.
     * @param outputChannel byte channel to write to.
     * @param startPosition source file offset.
     * @param options options for opening source file.
     * @return sum of bytes written.
     * @throws IOException thrown if error during reading or writing occured.
     */
    public static long writeBytes(Path source, WritableByteChannel outputChannel, long startPosition, StandardOpenOption... options) throws IOException  {
        long size = getBasicAttributes(source).size();
        long count = 0;
        try (FileChannel inputChannel = FileChannel.open(source, options)) {
            long fileCount = startPosition;
            count = 0;
            while (fileCount < size) {
                long transferCount = inputChannel.transferTo(fileCount, size-fileCount, outputChannel);
                if (transferCount <= 0){
                    break;
                }
                fileCount += transferCount;
                count += transferCount;
            }
        }
        return count;
    }

    /**
     * Reads text from byte channel to byte buffer in given charset.
     * @param channel channel to read from.
     * @param charset charset of text.
     * @param readBuffer destination buffer for reading.
     * @return read text
     * @throws IOException thrown if error during reading occured.
     */
    public static String readText(ReadableByteChannel channel, Charset charset, ByteBuffer readBuffer) throws IOException {
        String result = EMPTY;
        channel.read(readBuffer);
        readBuffer.flip();
        if (readBuffer.hasRemaining()) {
            byte[] bytes = new byte[readBuffer.limit()];
            readBuffer.get(bytes);
            result = new String(bytes, charset);
        }
        readBuffer.compact();
        return result;
    }

    /**
     * Reads text from byte channel to output file with open options in given charset.
     * @param channel channel to read from.
     * @param path path to output file to write.
     * @param charset charset for reading from channel.
     * @param readBuffer buffer for processing input.
     * @param options open options for output file.
     * @return sum of read bytes.
     * @throws IOException thrown if error occured during reading or writing.
     */
    public static long readText(ReadableByteChannel channel, Path path, Charset charset, ByteBuffer readBuffer, StandardOpenOption... options)  throws IOException {
        long c = 0;
        try (BufferedWriter out = Files.newBufferedWriter(path, charset, options)) {
            while (channel.read(readBuffer) != -1) {
                readBuffer.flip();
                while (readBuffer.hasRemaining()) {
                    out.write(readBuffer.get());
                    c++;
                }
                readBuffer.compact();
            }
        }
        readBuffer.clear();
        return c;
    }

    /**
     * Writes text from given source file to byte channel.
     * @param source path of source file.
     * @param outputChannel output channel to write to.
     * @param startPosition source file offset.
     * @param charset charset for reading file.
     * @param byteBuffer for output processing.
     * @return sum of read bytes.
     * @throws IOException thrown if error during reading or writing occured.
     */
    public static long writeText(Path source, WritableByteChannel outputChannel, long startPosition, Charset charset, ByteBuffer byteBuffer) throws IOException {
        byte[] buf = new byte[0];
        try (BufferedReader in = Files.newBufferedReader(source, charset)) {
            buf = new byte[(int)getBasicAttributes(source).size() - (int)startPosition];
            int i = 0;
            in.skip(startPosition);
            int c = in.read();
            while (c != -1) {
                buf[i++] = (byte)c;
                c = in.read();
            }
            writeByteArray(outputChannel, byteBuffer, buf);
        }
         return buf.length;
    }

    /**
     * Writes string array to byte channel.
     * @param channel output byte channel.
     * @param charset charset output.
     * @param writeBuffer byte buffer for output processing.
     * @param output string array for output.
     * @throws IOException thrown if error during writing occured.
     */
    public static void writeText(WritableByteChannel channel, Charset charset, ByteBuffer writeBuffer, String... output) throws IOException {
        for (String output1 : output) {
            writeString(channel, charset, writeBuffer, output1);
        }
    }

    /**
     * Writes string to byte channel in charset.
     * @param channel output byte channel.
     * @param charset charset for encoding output.
     * @param writeBuffer byte buffer used for output processing.
     * @param string string to output.
     * @throws IOException thrown if error during output occured.
     */
    public static void writeString(WritableByteChannel channel, Charset charset, ByteBuffer writeBuffer, String string) throws IOException {
        writeBuffer.put(charset.encode(string));
        writeBuffer.flip();
        channel.write(writeBuffer);
        writeBuffer.compact();
    }

    /**
     * Returns BasicFileAttributes of given file.
     * @param path path of file.
     * @return BasicFileAttribute object.
     * @throws IOException thrown if file does not exist or can not be open.
     */
    public static BasicFileAttributes getBasicAttributes(Path path) throws IOException  {
        return Files.readAttributes(path, BasicFileAttributes.class);
    }
    public static void writeSystemMessage(String message) {
        System.out.println(message);
        uiOutput(message);
    }
    public static void addTextFieldOutput(TextInputControl textOutput) {
        synchronized(UIOUTPUTFIELDS) {
            UIOUTPUTFIELDS.add(textOutput);
        }
    }
    private static void uiOutput(String message) {
        UIOUTPUTFIELDS.forEach((e) -> 
                e.appendText(message.concat(LINESEPARATOR))
        );
    }
}
