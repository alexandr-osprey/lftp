package org.lftp.connection;

import java.io.*;
import java.net.*;
import java.nio.channels.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.*;
import org.lftp.ioprocessing.IOProcessing;

/**
 * Class listens for incoming data connections on specified address and port.
 * @author alexandr-osprey
 */
public class DataConnectionsListener extends ConnectionsListener {
    private static final Map<InetAddress, Queue<FTPConnection>> HOSTCONNECTIONS;
    static {
        HOSTCONNECTIONS = Collections.synchronizedMap(new HashMap<>());
    }
    
    private DataConnectionsListener(InetAddress address, int port) {
        super(address, port);
    }

    /**
     * Adds an FTP connection to queue for listening for incoming data connections.
     * @param connection
     */
    public static void addConnection(FTPConnection connection) {
        InetAddress address = connection.remoteHost;
        if (!HOSTCONNECTIONS.containsKey(address)) {
            HOSTCONNECTIONS.put(address, new ArrayBlockingQueue<>(10, true));  
        }
        HOSTCONNECTIONS.get(address).add(connection);
    }

    /**
     * Returns next FTP connection, waiting for incoming data connection.
     * @param address internet address of FTP connection.
     * @return FTP connection.
     */
    public static FTPConnection getNextConnection(InetAddress address) {
        Queue<FTPConnection> queue = HOSTCONNECTIONS.get(address);
        FTPConnection c = queue.poll();
        if (queue.isEmpty()) {
            HOSTCONNECTIONS.remove(address);
        }
        return c;
    }
    
    

    /**
     * Factory method for creating Data connection listener.
     * @param address internet address for listening.
     * @param port port number for listening.
     * @return Data connection listener
     */
    public static ConnectionsListener getListener(InetAddress address, int port) {
        return new DataConnectionsListener(address, port);
    }

    /**
     * Listens for expected incoming data connections from known hosts.
     */
    @Override
    public void run() {
        Thread.currentThread().setName(NAME);
        try {
            LOGGER.entering(NAME, "run");
            serverChannel = ServerSocketChannel.open();
            serverChannel.bind(new InetSocketAddress(address, port));
            while (isRunning()) {
                SocketChannel s = serverChannel.accept();
                InetAddress a = ((InetSocketAddress)s.getRemoteAddress()).getAddress();
                if (HOSTCONNECTIONS.containsKey(a)) {
                    getNextConnection(a).setDataChannel(s);
                } else {
                    s.close();
                }
            }
        } catch (ClosedByInterruptException e) {
        } catch (AsynchronousCloseException e) {
        } catch (IOException e) {
            IOProcessing.writeSystemMessage(e.getMessage());
            LOGGER.log(Level.SEVERE, "Error durung connecting. ", e);
        }
        LOGGER.exiting(NAME, "run");
    }
    //@Override
    public static void clear() {
        HOSTCONNECTIONS.clear();
    }
}
