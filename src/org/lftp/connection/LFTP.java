package org.lftp.connection;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.Properties;
import java.util.concurrent.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.*;
import org.lftp.commands.vfs.VirtualFS;
import org.lftp.ioprocessing.IOProcessing;
import org.lftp.misc.MaintanceTools;
import org.lftp.misc.User;

/**
 * Main application class.
 * @author alexandr-osprey
 */
public class LFTP extends Application {
    // static
    private static InetAddress LOCAL4;
    private static InetAddress LOCAL6;
    private static int CONTROLPORT = 21;
    public static int DATAPORT = 20;
    private static ExecutorService REQUESTSPOOL;
    private boolean serverRunning = false;
    private static final String CONFIGPATH = "config.properties";
    private Properties loadedArgs;
    
    public static InetAddress getLocal4() {
        return LOCAL4;
    }
    public static InetAddress getLocal6() {
        return LOCAL6;
    }
    @Override
    public void start(Stage primaryStage) {
        initUI(primaryStage);
    }
    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
        } else {
            launchServer(args);
        }
    }
    private static boolean launchServer(String[] args) {
        if (args.length == 8) {
            try {
                if (Files.exists(Paths.get(args[0]))) {
                    VirtualFS.setHomePath(args[0]);
                } else {
                    System.err.println(args[0] + " does not exist. ");
                    return false;
                }
                if (Files.exists(Paths.get(args[1]))) {
                    User.setPathToData(args[1]);
                } else {
                    System.err.println(args[1] + " does not exist. ");
                    return false;
                }
                LOCAL4 = InetAddress.getByName(args[2]);
                LOCAL6 = InetAddress.getByName(args[3]);
                CONTROLPORT = Integer.parseInt(args[4]);
                DATAPORT = Integer.parseInt(args[5]);
                ControlConnectionsListener.MAXREQUESTS = Integer.parseInt(args[6]);
                MaintanceTools.setIdleMin(Integer.parseInt(args[7]));
            } catch (UnknownHostException | IllegalArgumentException e) {
                IOProcessing.writeSystemMessage(e.getMessage());
                return false;
            }
        } else {
            IOProcessing.writeSystemMessage("Wrong launch arguments number");
            return false;
        }
        REQUESTSPOOL = Executors.newFixedThreadPool(10);
        REQUESTSPOOL.execute(ControlConnectionsListener.getListener(LOCAL4, CONTROLPORT));
        REQUESTSPOOL.execute(ControlConnectionsListener.getListener(LOCAL6, CONTROLPORT));
        REQUESTSPOOL.execute(DataConnectionsListener.getListener(LOCAL4, DATAPORT));
        REQUESTSPOOL.execute(DataConnectionsListener.getListener(LOCAL6, DATAPORT));
        MaintanceTools.launchConnectionsSupervising();
        return true;
    }
    
    private void stopServer() {
        FTPConnection.closeAll();
        DataConnectionsListener.clear();
        ConnectionsListener.closeAll();
        REQUESTSPOOL.shutdownNow();
    }
    private void initUI(Stage primaryStage) {
        loadedArgs = loadConfig();
        
        Text homeDirectoryLabel = new Text("Home directory");
        TextField homeDirectoryField = new TextField();
        homeDirectoryField.setText(loadedArgs.getProperty("home"));
        DirectoryChooser homeChooser = new DirectoryChooser();
        Button homeChooseButton = new Button("Find");
        homeChooseButton.setOnAction((final ActionEvent e) -> {
            File file = homeChooser.showDialog(primaryStage);
            if (file != null) {
                homeDirectoryField.setText(file.getAbsolutePath());
            }
        });
        HBox homeHbox = new HBox();
        homeHbox.getChildren().add(homeDirectoryField);
        homeHbox.getChildren().add(homeChooseButton);
        homeHbox.setSpacing(5);
        
        Text userDataLocationLabel = new Text("User data location");
        TextField userDataLocationField = new TextField();
        userDataLocationField.setText(loadedArgs.getProperty("userData"));
        FileChooser userDataChooser = new FileChooser();
        Button userDataChooseButton = new Button("Find");
        userDataChooseButton.setOnAction((final ActionEvent e) -> {
            File file = userDataChooser.showOpenDialog(primaryStage);
            if (file != null) {
                userDataLocationField.setText(file.getAbsolutePath());
            }
        });
        HBox userDataHbox = new HBox();
        userDataHbox.getChildren().add(userDataLocationField);
        userDataHbox.getChildren().add(userDataChooseButton);
        userDataHbox.setSpacing(5);
        
        Text local4AddressLabel = new Text("Local IPv4");
        TextField local4AddressField = new TextField();
        local4AddressField.setText(loadedArgs.getProperty("local4"));
        
        Text local6AddressLabel = new Text("Local IPv6");
        TextField local6AddressField = new TextField();
        local6AddressField.setText(loadedArgs.getProperty("local6"));
        
        Text controlPortLabel = new Text("Control port");
        TextField controlPortField = new TextField();
        controlPortField.setText(loadedArgs.getProperty("controlPort"));
        
        Text dataPortLabel = new Text("Data port");
        TextField dataPortField = new TextField();
        dataPortField.setText(loadedArgs.getProperty("dataPort"));
        
        Text maxConnectionsLabel = new Text("Number of simultaneous connections");
        TextField maxConnectionsField = new TextField();
        maxConnectionsField.setText(loadedArgs.getProperty("maxConnections"));
        
        Text idleMinutesLabel = new Text("Timeout");
        TextField idleMinutesField = new TextField();
        idleMinutesField.setText(loadedArgs.getProperty("idleMinutes"));
        
        Button launchStopButton = new Button("Launch server");
        launchStopButton.setMaxWidth(Double.MAX_VALUE);
        launchStopButton.setOnAction((final ActionEvent e) -> {
            if (!serverRunning) {
                String[] args = new String[] {
                    homeDirectoryField.getText(),
                    userDataLocationField.getText(),
                    local4AddressField.getText(),
                    local6AddressField.getText(),
                    controlPortField.getText(),
                    dataPortField.getText(),
                    maxConnectionsField.getText(),
                    idleMinutesField.getText()
                };
                if (launchServer(args)) {
                    serverRunning = true;
                    saveConfig(args);
                    launchStopButton.setText("Stop server");
                    launchStopButton.setStyle("-fx-background-color: CRIMSON;");
                    IOProcessing.writeSystemMessage("Server started");
                }
            } else {
                serverRunning = false;
                stopServer();
                launchStopButton.setText("Launch server");
                launchStopButton.setStyle("-fx-background-color: LIGHTGREEN;");
                IOProcessing.writeSystemMessage("Server stopped");
            }
        });
        launchStopButton.setStyle("-fx-background-color: LIGHTGREEN;");
        
        TextArea systemOutput = new TextArea();
        systemOutput.setEditable(false);
        systemOutput.setMaxHeight(300);
        IOProcessing.addTextFieldOutput(systemOutput);
        
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(20);
        
        gridPane.add(homeDirectoryLabel, 0, 0);
        gridPane.add(homeHbox, 1, 0);
        gridPane.add(userDataLocationLabel, 0, 1);
        gridPane.add(userDataHbox, 1, 1);
        gridPane.add(local4AddressLabel, 0, 2);
        gridPane.add(local4AddressField, 1, 2);
        gridPane.add(local6AddressLabel, 0, 3);
        gridPane.add(local6AddressField, 1, 3);
        gridPane.add(controlPortLabel, 0, 4);
        gridPane.add(controlPortField, 1, 4);
        gridPane.add(dataPortLabel, 0, 5);
        gridPane.add(dataPortField, 1, 5);
        gridPane.add(maxConnectionsLabel, 0, 6);
        gridPane.add(maxConnectionsField, 1, 6);
        gridPane.add(idleMinutesLabel, 0, 7);
        gridPane.add(idleMinutesField, 1, 7);
        
        VBox pane = new VBox();
        pane.setPadding(new Insets(10));
        pane.setMinSize(500, 600);
        pane.setSpacing(20.0);
        pane.setCenterShape(true);
        
        ObservableList<Node> children = pane.getChildren();
        children.add(gridPane);
        children.add(launchStopButton);
        children.add(systemOutput);
        
        Scene scene = new Scene(pane);
        primaryStage.setOnCloseRequest(e -> { 
            Platform.exit();
            System.exit(0);
        });
        primaryStage.setTitle("LFTP Server");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private void saveConfig(String[] args) {
        try (OutputStream out = Files.newOutputStream(Paths.get(CONFIGPATH))) {
            Properties prop = new Properties();
            prop.setProperty("home", args[0]);
            prop.setProperty("userData", args[1]);
            prop.setProperty("local4", args[2]);
            prop.setProperty("local6", args[3]);
            prop.setProperty("controlPort", args[4]);
            prop.setProperty("dataPort", args[5]);
            prop.setProperty("maxConnections", args[6]);
            prop.setProperty("idleMinutes", args[7]); 
            prop.store(out, null);
        } catch (IOException e) {}
    }
    private Properties loadConfig() {
        Properties prop = new Properties();
        try (InputStream in = Files.newInputStream(Paths.get(CONFIGPATH))) {
            prop.load(in);
        } catch (IOException e) {}
        return prop;
    }
}
