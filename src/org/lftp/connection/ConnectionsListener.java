/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lftp.connection;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.channels.ServerSocketChannel;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.*;
import org.lftp.ioprocessing.IOProcessing;

/**
 *
 * @author operator
 */
public abstract class ConnectionsListener implements Runnable {
    protected final static Set<ConnectionsListener> ACTIVELISTENERS;
    protected ServerSocketChannel serverChannel;
    protected final InetAddress address;
    protected final int port;
    protected static final Logger LOGGER;
    protected static ExecutorService REQUESTSPOOL;
    protected final String NAME;
    protected static int MAXREQUESTS;
    
    static {
        MAXREQUESTS = 100;
        ACTIVELISTENERS = Collections.synchronizedSet(new HashSet<>());
        REQUESTSPOOL = Executors.newFixedThreadPool(MAXREQUESTS);
        LOGGER = Logger.getLogger(ControlConnectionsListener.class.getName());
        LOGGER.setLevel(Level.FINER);
        try {
            LOGGER.addHandler(new FileHandler("ConnectionsListener.%u.%g.log", 1024*1024, 10, true));
        } catch (IOException e) {
            IOProcessing.writeSystemMessage(e.getMessage());
        }
    }
    
    protected ConnectionsListener(InetAddress address, int port) {
        this.address = address;
        this.port = port;
        this.NAME = String.format("Connection listener: %s:%d", address, port);
        ACTIVELISTENERS.add(this);
        if (REQUESTSPOOL.isShutdown()) {
            REQUESTSPOOL = Executors.newFixedThreadPool(MAXREQUESTS);
        }
    }
    
    protected Thread getThread() {
        return Thread.currentThread();
    }
    protected boolean isRunning() {
        return !getThread().isInterrupted();
    }

    @Override
    public String toString() {
        return NAME;
    }
    
    protected static void closeAll() {
        synchronized(ACTIVELISTENERS) {
            ACTIVELISTENERS.forEach((e) -> close(e));
        }
        REQUESTSPOOL.shutdownNow();
    }
    
    protected static void close(ConnectionsListener l) {
        Thread t = l.getThread();
        t.interrupt();
        try {
            t.join();
        } catch (InterruptedException e1) {}
        t.interrupt();
        try {
            l.serverChannel.configureBlocking(false);
            l.serverChannel.close();
        } catch (IOException e) {}
    }
}
