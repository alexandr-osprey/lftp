package org.lftp.connection;

import java.util.logging.*;
import java.io.*;
import java.net.*;
import java.nio.channels.*;
import org.lftp.ioprocessing.IOProcessing;

class ControlConnectionsListener extends ConnectionsListener {
    private ControlConnectionsListener(InetAddress address, int port) {
        super(address, port);
    }
    public static ConnectionsListener getListener(InetAddress address, int port) {
        return new ControlConnectionsListener(address, port);
    }
    
    @Override
    public void run() {
        Thread.currentThread().setName(NAME);
        try {
            LOGGER.entering(NAME, "run");
            serverChannel = ServerSocketChannel.open();
            serverChannel.bind(new InetSocketAddress(address, port));
            while (isRunning()) {
                FTPConnection c = FTPConnection.getFTPConnection(serverChannel.accept());
                REQUESTSPOOL.submit(c);
                LOGGER.logp(Level.FINEST, NAME, "run", "Accepted: {0}", c.toString());
            }
        } catch (ClosedByInterruptException e) {
        } catch (AsynchronousCloseException e) {
        } catch (IOException e) {
            IOProcessing.writeSystemMessage(e.getMessage());
            LOGGER.log(Level.SEVERE, "Error durung binding. ", e);
        }
        LOGGER.exiting(NAME, "run");
    }
}
