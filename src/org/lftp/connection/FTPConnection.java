package org.lftp.connection;

import org.lftp.ioprocessing.IOProcessing;
import org.lftp.misc.User;
import org.lftp.commands.Command;
import org.lftp.commands.vfs.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.logging.*;

/**
 * Main class, representing FTP connection.
 * @author alexandr-osprey
 */
public class FTPConnection implements Runnable {

    /**
     * Set for storing all active FTP connections
     */
    private static final Set<FTPConnection> ACTIVECONNECTIONS;
    private static final Logger LOGGER;
    static {
        ACTIVECONNECTIONS = Collections.synchronizedSet(new HashSet<>());
        LOGGER = Logger.getLogger(FTPConnection.class.getName());
        LOGGER.setLevel(Level.FINEST);
        try {
            LOGGER.addHandler(new FileHandler(
                    FTPConnection.class.getSimpleName().concat(".%u.%g.log"), 1024*1024, 10, true));
        } catch (IOException e) {
            IOProcessing.writeSystemMessage(e.getMessage());
            System.err.println(e);
        }
    }

    public static void closeIdle(ChronoUnit idleUnit, int idleAmount) {
        synchronized(ACTIVECONNECTIONS) {
            ACTIVECONNECTIONS.stream().filter(
                    (e) -> (e.lastAction.until(Instant.now(), idleUnit) > idleAmount &&
                            !e.isDataConnected())).forEach((e) -> close(e));
        }
    }
    
    public static void closeAll() {
        synchronized(ACTIVECONNECTIONS) {
            ACTIVECONNECTIONS.stream().forEach((e) -> close(e));
        }
    }
    
    public static void close(FTPConnection c) {
        Thread t = c.getThread();
        t.interrupt();
        try {
            t.join();
        } catch (InterruptedException e1) {}
        t.interrupt();
        c.closeControlChannel();
        c.closeDataChannel();
    }
    
    // final fields
    public final SocketChannel controlChannel;
    public final InetAddress remoteHost;
    public final int port;
    public final ByteBuffer BYTEBUFFER;
    public VirtualFS workingDirectory;
    public Charset charset;
    public STRU dataStru;
    public TYPE reprType;
    public MODE mode;
    public boolean EPSVALL;
    public String renameFrom;
    public final String NAME;
    
    // non final fields
    private SocketChannel dataChannel;
    private long restFrom;
    private User user;
    private String userLogin;
    private Locale currentLocale;
    private ResourceBundle messages;
    private Instant lastAction;
    
    public static enum TYPE {
        ASCII, 
        BINARY
    }
    public static enum STRU {
        FILE
    }
    public static enum MODE {
        STREAM
    }
    private FTPConnection(SocketChannel socketChannel) {
        this.controlChannel = socketChannel;
        this.BYTEBUFFER = ByteBuffer.allocate(64*1024);
        this.remoteHost = socketChannel.socket().getInetAddress();
        this.port = socketChannel.socket().getPort();
        NAME = String.format("FTP connection %s:%d",  this.remoteHost, this.port);
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        if (this.user == null) {
            this.user = user;
        }
    }
    public String getUserLogin() {
        return userLogin;
    }
    public void setUserLogin(String userLogin) {
        if (this.userLogin.isEmpty()) {
            this.userLogin = userLogin;
        } 
    }
    //overriden methods

    /**
     * Starts listening for user input and processing it.
     */
    @Override
    public void run() {
        synchronized(ACTIVECONNECTIONS) {
            ACTIVECONNECTIONS.add(this);
        }
        Thread.currentThread().setName(NAME);
        LOGGER.entering(NAME, "run");
        try {
            sendGreetings();
            while (isRunning()) {
                String input = readInput();
                if (!check(input)) {
                    continue;
                }
                LOGGER.logp(Level.FINEST, this.toString(), "run", "Input: {0}", input);
                lastAction = Instant.now();
                Command command = Command.getByInput(this, input);
                if (command == null) {
                    writeAnswer(500, "commandUnrecognized");
                    continue;
                }
                command.execute();
            }
        } catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("IO error in run method %s", this.toString()), e);
            System.exit(-1);
        }
        synchronized(ACTIVECONNECTIONS) {
            ACTIVECONNECTIONS.remove(this);
        }
        LOGGER.exiting(NAME, "run");
    }
    @Override
    public String toString() {
        return String.format("%s %s %s:%d", FTPConnection.class.getSimpleName(), userLogin, remoteHost, port);
    }
    public String writeAnswer(int code, String key) {
        return writeAnswer(code, key, IOProcessing.EMPTY);
    }
    public String writeAnswer(int code, String key, String opt) {
        return writeAnswer(String.format("%d %s%s. ", code, getMessage(key), opt));
    }
    public String writeMultilineAnswer(int code, String key, String... content) {
        String answer = String.format("%d- %s%s", code, getMessage(key), IOProcessing.LINESEPARATOR);
        for (String line: content) {
            answer = answer.concat(" " + line + IOProcessing.LINESEPARATOR);
        }
        answer = answer.concat(String.format("%d %s.", code, getMessage("end")));
        return writeAnswer(answer);
    }
    private String writeAnswer(String answer) {
        try {
            if (answer != null && !answer.equals(IOProcessing.EMPTY)) {
                IOProcessing.writeText(controlChannel, charset, BYTEBUFFER, answer.concat(IOProcessing.LINESEPARATOR));
                LOGGER.logp(Level.FINEST, this.toString(), "writeAnswer", answer);
            }
        } catch (AsynchronousCloseException e) {
            getThread().interrupt();
        } catch (IOException e) {
            IOProcessing.writeSystemMessage(e.getMessage());
            LOGGER.log(Level.INFO, String.format("Error writing to output stream %s", this.toString()), e);
        }
        return answer;
    }
    //instance methods
    public void reinitialize() {
        setDefaultParameters(this);
    }
    private boolean check(String input) {
        if (input == null || input.length() <= 0) {
            return false;
        }
        String[] tokens = input.split(" ");
        for (String s : tokens) {
            for (char c : s.toCharArray()) {
                if (!Character.isValidCodePoint(c)) {
                    writeAnswer(500, "syntaxError");
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Gets restarting position, set by REST command.
     * @return restarting position in bytes.
     */
    public long getRestFrom() {
        long l = restFrom;
        restFrom = 0;
        return l;
    }

    /**
     * Get current's FTP connection thread.
     * @return Thread object.
     */
    public Thread getThread() {
        return Thread.currentThread();
    }
    public boolean isRunning() {
        return !getThread().isInterrupted();
    }

    /**
     * Sets restarting position in bytes.
     * @param l bytes to skip.
     */
    public void setRestFrom(long l) {
        restFrom = l;
    }
    private String readInput() {
        try {
            if (controlChannel.isOpen()) {
                String input = IOProcessing.readText(controlChannel, charset, BYTEBUFFER).trim();
                return input;
            }
        } catch (AsynchronousCloseException e) {
            getThread().interrupt();
        } catch (IOException e) {
            IOProcessing.writeSystemMessage(e.getMessage());
            LOGGER.log(Level.INFO, String.format("Error reading from input stream %s", this.toString()), e);
        }
        return IOProcessing.EMPTY;
    }
    private void sendGreetings() throws IOException {
        writeAnswer(220, "serviceReady");
    }

    /**
     * Checks whether FTP connection has data connection.
     * @return true, if data connected, false otherwise.
     */
    public synchronized boolean isDataConnected() {
        if (dataChannel != null) {
             return dataChannel.isConnected();
        } else {
            return false;
        }
    }

    /**
     * Closes current FTP connection by closing it's control connection, data connection, interrupting thread.
     */
    public void closeControlChannel() {
        try {
            controlChannel.close();
        } catch (IOException e) {}
    }
    public void closeDataChannel() {
        try {
            if (dataChannel != null) {
                dataChannel.close();
            }
        } catch(IOException e) { }
    }

    /**
     * Associates given socket channel object with this FTP connection.
     * @param socketChannel
     */
    public void setDataChannel(SocketChannel socketChannel) {
        dataChannel = socketChannel;
    }
    public SocketChannel getDataChannel() {
        return dataChannel;
    }

    /**
     * Checks if FTP connection has file structure and binary type set.
     * @return true if file and binary, false otherwise.
     */
    public boolean isFileAndBinary() {
        return dataStru == STRU.FILE && reprType == TYPE.BINARY;
    }

    /**
     * Checks if FTP connection has file structure and ASCII type set.
     * @return true if file and ASCII, false otherwise.
     */
    public boolean isFileAndASCII() {
        return dataStru == STRU.FILE && reprType == TYPE.ASCII;
    }

    /**
     * Sets language and country for commands and text file processing.
     * @param language RU or US allowed
     * @param country RU or EN allowed
     */
    public void setLocale(String language, String country) {
        currentLocale = new Locale(language, country);
        messages = ResourceBundle.getBundle("org.lftp.misc.MessagesBundle", currentLocale);
    }
    public String getMessage(String key) {
        return messages.getString(key);
    }
    // static methods

    /**
     * Returns a FTP connection based on given SocketChannel.
     * @param socketChannel connected to user.
     * @return newly created FTP connection.
     */
    public static FTPConnection getFTPConnection(SocketChannel socketChannel) {
        FTPConnection c = new FTPConnection(socketChannel);
        setDefaultParameters(c);
        return c;
    }
    private static void setDefaultParameters(FTPConnection connection) {
        connection.restFrom = 0;
        connection.charset = Charset.forName("UTF-8");
        connection.reprType = TYPE.BINARY;
        connection.dataStru = STRU.FILE;
        connection.BYTEBUFFER.clear();
        connection.mode = MODE.STREAM;
        connection.userLogin = IOProcessing.EMPTY;
        connection.user = null;
        connection.EPSVALL = false;
        connection.renameFrom = IOProcessing.EMPTY;
        connection.workingDirectory = null;
        connection.lastAction = Instant.now();
        connection.setLocale("en", "US");
    }
}
