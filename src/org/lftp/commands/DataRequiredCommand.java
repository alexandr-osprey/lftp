package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.nio.channels.*;
import java.nio.file.*;
import java.time.*;

abstract class DataRequiredCommand extends LoginRequiredCommand{
    protected SocketChannel dataChannel;
    Path path;
    StandardOpenOption[] options;
    
    protected DataRequiredCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected boolean prepare() {
        return super.prepare() && waitForDataConnection();
    }
    protected boolean waitForDataConnection() {
        if (!connection.isDataConnected()) {
            LocalDateTime end = LocalDateTime.now().plusSeconds(4);
            while (!connection.isDataConnected()) {
                if (end.compareTo(LocalDateTime.now()) < 0) {
                    break;
                }
            }
            if (!connection.isDataConnected()) {
                connection.closeDataChannel();
                connection.writeAnswer(425, "dataConnectionError");
                return false;
            }
        }
        dataChannel = connection.getDataChannel();
        connection.writeAnswer(125, "transferStarting");
        return true;
    }
}
