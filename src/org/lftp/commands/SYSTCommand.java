package org.lftp.commands;

import org.lftp.connection.FTPConnection;

class SYSTCommand extends Command {
    protected SYSTCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override 
    protected void performAction() {
        connection.writeAnswer(215, "empty", System.getProperty("os.name"));
    }
}
