package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import org.lftp.ioprocessing.IOProcessing;
import java.io.*;
import java.nio.file.*;
import static java.nio.file.StandardOpenOption.*;

class APPECommand extends STORCommand {
    protected APPECommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected boolean prepare() {
        if (!super.prepare()) {
            return false;
        }
        try {
            if (!Files.exists(path)) {
                Files.createDirectories(path.getParent());
            }
            connection.setRestFrom(IOProcessing.getBasicAttributes(path).size());
            options = new StandardOpenOption[]{WRITE, CREATE, APPEND };
        } catch (IOException e) {
            connection.writeAnswer(450, "pathInvalid");
            return false;
        }
        return true;
    }
    
}
