package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.io.IOException;
import java.nio.file.*;
import java.util.logging.Level;
import static org.lftp.commands.Command.LOGGER;

class RMDCommand extends LoginRequiredCommand {
    protected RMDCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        try {
            connection.workingDirectory.rmdCommand(argument);
            connection.writeAnswer(250, "rmdSuccess");
        } catch (NoSuchFileException e) {
            connection.writeAnswer(501, "directoryNotFound");
        }
        catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("IO error in %s %s", RMDCommand.class.getSimpleName(), connection.toString()), e);
            connection.writeAnswer(450, "ioError");
        }
    }
}
