package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.nio.file.*;

class CWDCommand extends LoginRequiredCommand {
    protected CWDCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        try {
            connection.writeAnswer(200, "cwdSuccess", connection.workingDirectory.cwdCommand(argument));
        } catch (NoSuchFileException e) {
            connection.writeAnswer(550, "directoryNotFound");
        }
    }
}
