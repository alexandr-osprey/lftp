package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.time.LocalDateTime;

class ABORCommand extends LoginRequiredCommand {
    protected ABORCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        System.out.println(String.format("abor %s, %s: ", LocalDateTime.now(), connection.toString()));
        if (connection.isDataConnected()) {
            connection.closeDataChannel();
            connection.writeAnswer(426, "dataConnectionClosed");
        }
        connection.writeAnswer(226, "aborSuccess");
    }
}
