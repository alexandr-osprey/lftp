package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import org.lftp.ioprocessing.IOProcessing;
import java.io.*;
import java.nio.channels.*;
import java.nio.file.*;
import static java.nio.file.StandardOpenOption.*;
import java.util.logging.Level;
import static org.lftp.commands.Command.LOGGER;

class STORCommand extends DataRequiredCommand {
    protected STORCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected boolean prepare() {
        if (!super.prepare()) {
            return false;
        }
        path = connection.workingDirectory.toPathNonExistent(argument);
        options = new StandardOpenOption[] {WRITE, CREATE, TRUNCATE_EXISTING };
        try {
            if (!Files.exists(path)) {
                Files.createDirectories(path.getParent());
            }
        } catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("IO error in %s %s", STORCommand.class.getSimpleName(), connection.toString()), e);
            connection.writeAnswer(450, "ioError");
        }
        return true;
    }
    @Override
    protected void performAction() {
        try {
            long stored;
            if (connection.isFileAndBinary()) {
                stored = IOProcessing.readBytes(dataChannel, path, connection.getRestFrom(), options);
            } else {
                stored = IOProcessing.readText(dataChannel, path, connection.charset,
                        connection.BYTEBUFFER, options);
            }
            connection.writeAnswer(226, "storSuccess", Long.toString(stored));
        } catch (ClosedChannelException e) {
            System.err.println(connection.toString() + " error writing to closed channel. ");
        }catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("IO error in %s %s", STORCommand.class.getSimpleName(), connection.toString()), e);
            connection.writeAnswer(450, "ioError");
        } finally {
            connection.closeDataChannel();
        }
    }
}
