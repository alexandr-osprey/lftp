package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import org.lftp.connection.FTPConnection.TYPE;

class TYPECommand extends LoginRequiredCommand {
    protected TYPECommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        // type parameters need to implement N T C
        switch (argument.toUpperCase()) {
            case "I":
                connection.reprType = TYPE.BINARY;
                break;
            case "A":
                connection.reprType = TYPE.ASCII;
                break;
            default:
                connection.writeAnswer(504, "parameterUnknown");
                return;
        }
        connection.writeAnswer(200, "modeSuccess");
    }
}
