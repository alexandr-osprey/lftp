package org.lftp.commands;

import java.io.*;
import java.util.logging.*;
import org.lftp.connection.FTPConnection;
import org.lftp.ioprocessing.IOProcessing;

/**
 * Command class exposes methods for obtaining and executing commands. Superclass for all other commands.
 * @author alexandr-osprey
 */
public abstract class Command {
    private static final String PACKAGENAME = Command.class.getPackage().getName();
    protected String argument;
    protected FTPConnection connection;
    protected final String LINESEPARATOR = System.getProperty("line.separator");
    protected static final Logger LOGGER;
    static {
        LOGGER = Logger.getLogger(Command.class.getName());
        LOGGER.setLevel(Level.FINEST);
        try {
            LOGGER.addHandler(new FileHandler(Command.class.getSimpleName().concat(".%u.%g.log"), 1024*1024, 10, true));
        } catch (IOException e) {
            IOProcessing.writeSystemMessage(e.getMessage());
        }
    }

    protected Command(FTPConnection connection, String argument) {
        this.argument = argument;
        this.connection = connection;
    }

    /**
     * Method for preparing before performing action and checking all preconditions. 
     * @return false, if action can not be performed
     */
    protected boolean prepare() {
        return true;
    }

    /**
     * Method for performing action when all preconditions are met
     */
    public void execute() {
        if (prepare()) {
            performAction();
        }
    }

    /**
     * Method for overloading in child command classes.
     */
    protected abstract void performAction();

    /**
     * protected method for obtaining Command instances based on user input.
     * @param connection FTP connection associated with the command.
     * @param input user input.
     * @return Command instance or null if command not implemented
     */
    public static Command getByInput(FTPConnection connection, String input) {
        int i = input.indexOf(" ");
        String argument;
        String stringCommand;
        if (i != -1) {
            stringCommand = input.substring(0, i);
            argument = input.substring(i + 1);
        } else {
            stringCommand = input;
            argument = "";
        }
        String className = String.format("%s.%sCommand", PACKAGENAME, stringCommand.toUpperCase());
        try {
            Class commandClass = Class.forName(className);
            Command command = (Command)commandClass.getConstructor(FTPConnection.class,
                    String.class).newInstance(connection, argument);
            return command;
        } catch (ReflectiveOperationException e) {
            return null;
        }
    }
}
