package org.lftp.commands;

import org.lftp.connection.FTPConnection;


class RESTCommand extends LoginRequiredCommand {
    protected RESTCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        try {
            connection.setRestFrom(Long.parseLong(argument));
            connection.writeAnswer(350, "restSuccess", argument);
        } catch (NumberFormatException e) {
            connection.writeAnswer(501, "syntaxError");
        }
    }
}
