package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.io.IOException;
import java.nio.file.*;

class MKDCommand extends LoginRequiredCommand {
    protected MKDCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        try {
            String vPath = connection.workingDirectory.mkdCommand(argument);
            connection.writeAnswer(257, "mkdSuccess", vPath);
        } catch (FileAlreadyExistsException e) {
            connection.writeAnswer(501, "fileAlreadyExists");
        }
        catch (IOException e) {
            connection.writeAnswer(501, "syntaxError");
        }
    }
}
