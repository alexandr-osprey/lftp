package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.io.*;

class SIZECommand extends LoginRequiredCommand {
    protected SIZECommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        try {
           connection.writeAnswer(213, "empty", connection.workingDirectory.sizeCommand(argument)); 
        } catch (IOException e) {
            connection.writeAnswer(550, "fileNotRetriviable", argument);
        }
    }
}
