package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import org.lftp.misc.User;

class USERCommand extends Command {
    protected USERCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected boolean prepare() {
        if (connection.getUser() != null) {
            connection.writeAnswer(431, "passSuccess");
            return false;
        } else if (!User.exists(argument)) {
            connection.writeAnswer(432, "userNotFound");
            return false;
        }
        return true;
    }
    @Override
    protected void performAction() {
        connection.setUserLogin(argument);
        connection.writeAnswer(331, "userSuccess");
    }
}
