package org.lftp.commands;

import org.lftp.connection.DataConnectionsListener;
import org.lftp.connection.FTPConnection;
import static org.lftp.connection.LFTP.DATAPORT;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import static org.lftp.commands.Command.LOGGER;

class PASVCommand extends LoginRequiredCommand {
    protected PASVCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected boolean prepare() {
        if (!super.prepare()) {
            return false;
        }
        if (connection.EPSVALL) {
            connection.writeAnswer(503, "epsvAll");
            return false;
        }
        return true;
    }
    @Override
    protected void performAction() {
        try {
            DataConnectionsListener.addConnection(connection);
            String l = Inet4Address.getLocalHost().toString().replace('.', ',');
            l = l.substring(l.indexOf("/") + 1);
            String[] p = new String[2];
            String p0 = Integer.toString(DATAPORT);
            if (p0.length() > 2) {
                p[0] = p0.substring(0, 2);
                p[1] = p0.substring(2);
            } else {
                p[0] = "00";
                p[1] = p0;
            }
            connection.writeAnswer(227, "pasvSuccess", String.format("(%s,%s,%s)",
                    l, p[0], p[1]));
        } catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("IO error in %s %s", PASVCommand.class.getSimpleName(), connection.toString()), e);
            connection.writeAnswer(522, "dataConnectionError");
        }
    }
}
