package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import org.lftp.commands.vfs.VirtualFS;
import org.lftp.misc.User;

class PASSCommand extends Command {
    protected PASSCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    } 
    @Override
    protected boolean prepare() {
        if (connection.getUserLogin().isEmpty()) {
            connection.writeAnswer(503, "commandWrongSequence");
            return false;
        } 
        return true;
    }
    @Override
    protected void performAction() {
        User user = User.getUser(connection.getUserLogin(), argument);
        if (user != null) {
            connection.setUser(user);
            connection.workingDirectory = VirtualFS.newInstance(connection.getUserLogin());
            connection.writeAnswer(230, "passSuccess");
        } else {
            connection.writeAnswer(530, "wrongPassword");
        }
    }
}
