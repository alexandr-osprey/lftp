package org.lftp.commands;

import org.lftp.connection.FTPConnection;

class QUITCommand extends LoginRequiredCommand {
    protected QUITCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        while (connection.isDataConnected()) {}
        connection.writeAnswer(221, "quitSuccess");
        FTPConnection.close(connection);
    }
}
