package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import static org.lftp.connection.LFTP.*;
import java.io.*;
import java.net.*;
import java.nio.channels.*;
import java.util.logging.Level;
import java.util.regex.Pattern;
import static org.lftp.commands.Command.LOGGER;

class EPRTCommand extends LoginRequiredCommand {
    private String[] address;
    protected EPRTCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected boolean prepare() {
        if (!super.prepare()) {
            return false;
        }
        if (connection.EPSVALL) {
            connection.writeAnswer(425, "epsvAll");
            return false;
        }
        address = argument.substring(1).split(Pattern.quote("|"));
        if (address.length != 3) {
            connection.writeAnswer(501, "syntaxError");
            return false;
        } else if (!(address[0].equals("1") || address[0].equals("2"))) {
            connection.writeAnswer(522, "networkProtocolNotSupproted");
            return false;
        }
        return true;
    } 
    @Override
    protected void performAction() {
        int port = Integer.parseInt(address[2]);
        try {
            InetAddress remoteAddress = InetAddress.getByName(address[1]);
            boolean ipv6 = address[0].equals("2");
            SocketChannel channel = SocketChannel.open();
            channel.bind(new InetSocketAddress( ipv6 ? getLocal6() : getLocal4(), 0));
            channel.connect(new InetSocketAddress(remoteAddress, port));
            connection.setDataChannel(channel);
            connection.writeAnswer(200, "dataConnectionOpen");
        } catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("IO error in %s %s", EPRTCommand.class.getSimpleName(), connection.toString()), e);
            connection.writeAnswer(425, "dataConnectionError");
        }
    }
}
