package org.lftp.commands;

import org.lftp.connection.FTPConnection;

class REINCommand extends Command {
    protected REINCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    
    @Override
    protected void performAction() {
        connection.reinitialize();
        connection.writeAnswer(220, "serviceReady");
    }
}
