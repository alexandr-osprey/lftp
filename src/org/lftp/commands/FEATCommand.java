package org.lftp.commands;

import org.lftp.connection.FTPConnection;

class FEATCommand extends Command { 
    protected FEATCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        connection.writeMultilineAnswer(211, "feat", 
                "UTF8", "TVFS", "FEAT", "MDTM", "SIZE", "REST STREAM", "nat6");
    }
}
