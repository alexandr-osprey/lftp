package org.lftp.commands;

import org.lftp.connection.FTPConnection;

class RNFRCommand extends LoginRequiredCommand {
    protected RNFRCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        if (connection.workingDirectory.fileExists(argument)) {
            connection.renameFrom = argument;
            connection.writeAnswer(350, "rnfrSuccess");
        } else {
            connection.writeAnswer(550, "fileNotFound");
        }
    }
}
