package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.io.*;

class MDTMCommand extends LoginRequiredCommand {
    protected MDTMCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        try {
           connection.writeAnswer(213, "empty", connection.workingDirectory.mdtmCommand(argument)); 
        } catch (IOException e) {
            connection.writeAnswer(550, "fileNotRetriviable", argument);
        }
    }
}
