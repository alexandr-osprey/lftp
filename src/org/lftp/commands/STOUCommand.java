package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.util.UUID;

class STOUCommand extends STORCommand {
    protected STOUCommand(FTPConnection connection, String argument) {
        super(connection, argument);    
    }
    @Override
    protected boolean prepare() {
        if (!super.prepare()) {
            return false;
        }
        path = connection.workingDirectory.toPathNonExistent(UUID.randomUUID().toString());
        return true;
    }
}
