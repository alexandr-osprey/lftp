package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.nio.charset.Charset;

class OPTSCommand extends LoginRequiredCommand {
    protected OPTSCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        String[] tokens = argument.split(" ");
        switch (tokens[0].toUpperCase()) {
            case "UTF8": {
                switch (tokens[1].toUpperCase()) {
                    case "ON":
                        connection.writeAnswer(200, "utf8Enabled");
                        connection.charset = Charset.forName("UTF-8");
                        break;
                    case "OFF":
                        connection.writeAnswer(200, "utf8Disabled");
                        connection.charset = Charset.forName("ASCII");
                        break;
                    default:
                        connection.writeAnswer(501, "parameterUnknown");
                        break;
                }
                break;
            }
            default:
                connection.writeAnswer(501, "parameterUnknown");
                break;
        }
    }
}
