package org.lftp.commands;

import org.lftp.connection.DataConnectionsListener;
import org.lftp.connection.FTPConnection;
import static org.lftp.connection.LFTP.DATAPORT;

class EPSVCommand extends LoginRequiredCommand {
    protected EPSVCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        if (argument.toUpperCase().equals("ALL")) {
            connection.EPSVALL = true;
            connection.writeAnswer(220, "epsvAll");
            return;
        }
        DataConnectionsListener.addConnection(connection);
        connection.writeAnswer(229, "epsvSuccess", String.format("(|%s|%s|%d|)", "", "", DATAPORT));
    }
}
