package org.lftp.commands;

import org.lftp.connection.FTPConnection;

class MODECommand extends LoginRequiredCommand {
    protected MODECommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        switch (argument.toUpperCase()) {
            case "S":
                connection.mode = FTPConnection.MODE.STREAM;
                connection.writeAnswer(200, "streamModeSet");
                break;
            default:
                connection.writeAnswer(504, "parameterUnknown");
                break;
        }
    }
}
