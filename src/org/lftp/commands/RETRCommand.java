package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import org.lftp.ioprocessing.IOProcessing;
import java.io.*;
import java.nio.channels.*;
import java.nio.file.*;
import static java.nio.file.StandardOpenOption.*;
import java.util.logging.Level;
import static org.lftp.commands.Command.LOGGER;


class RETRCommand extends DataRequiredCommand {
    protected RETRCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected boolean prepare() {
        if (!super.prepare()) {
            return false;
        }
        try {
            path = connection.workingDirectory.toPath(argument);
            options = new StandardOpenOption[]{READ};
        } catch (NoSuchFileException e) {
            connection.writeAnswer(550, "fileNotFound", argument);
            return false;
        }
        if (Files.isDirectory(path)) {
            connection.writeAnswer(550, "fileNotFound", argument);
            return false;
        }
        return true;
    }
    @Override
    protected void performAction() {
        try {
            long stored;
            if (connection.isFileAndBinary()) {
                stored = IOProcessing.writeBytes(path, dataChannel, connection.getRestFrom(), options);
            } else {
                stored = IOProcessing.writeText(path, dataChannel, connection.getRestFrom(), connection.charset,  connection.BYTEBUFFER);
            }
            connection.writeAnswer(226, "retrSuccess", Long.toString(stored));
        } catch (ClosedChannelException e) {
            System.err.println(connection.toString() + " error writing to closed channel. ");
        } catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("IO error in %s %s", RETRCommand.class.getSimpleName(), connection.toString()), e);
            connection.writeAnswer(450, "ioError");
        } finally {
            connection.closeDataChannel();
        }
    }
}
