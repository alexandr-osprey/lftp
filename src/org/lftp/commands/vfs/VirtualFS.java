package org.lftp.commands.vfs;

import org.lftp.ioprocessing.IOProcessing;
import java.io.*;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.*;
import java.nio.file.attribute.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * VirtualFS class represents virtual file system. Class  Any file operations made via this class.
 * Any user able to perform any operations only in his own home directory. VirtualFS based on TVFS requirements (UNIX-like).
 * @author alexandr-osprey.
 */
public class VirtualFS {
    private VFSPath currentVFSPath;
    private final String username;
    
    private VirtualFS(String username) {
        this.username = username;
        currentVFSPath = VFSPath.newInstance(username);
    }

    /**
     * Creates new instance of VirtualFS based on username. 
     * @param username user login for operating in his own directory.
     * @return user specific VirtualFS instance.
     */
    public static VirtualFS newInstance(String username) {
        return new VirtualFS(username);
    }

    /**
     * Returns system dependent real path to curent VFSPath object.
     * @return real path to current VFSPath.
     */
    public String getRealPath() {
        return currentVFSPath.getRealPath();
    }

    /**
     * Return VFS path to current VFSPath object.
     * @return virtual path to current VFSPath.
     */
    public String getVirtualPath() {
        return currentVFSPath.getVirtualPath();
    }

    /**
     * Checks if file, specified by virtual path exists. If path is relative, it's resolved by current VFSPath.
     * @param virtualPath relative or absolute VFS Path.
     * @return true if file exists, false otherwise.
     */
    public boolean fileExists(String virtualPath) {
        try {
            VFSPath p = VFSPath.resolve(currentVFSPath, virtualPath);
            return true;
        } catch (NoSuchFileException e) {
            return false;
        }
    }
    
    public static void setHomePath(String home) {
        VFSPath.HOMEPATH = Paths.get(home);
    }

    /**
     * Returns Path object based on given VFS path.
     * @param virtualPath relative or absolute VFS path.
     * @return Path object, representing given file.
     * @throws NoSuchFileException thrown if given file does not exist.
     */
    public Path toPath(String virtualPath) throws NoSuchFileException {
        return VFSPath.resolve(currentVFSPath, virtualPath).getPathObject();
    }

    /**
     * Returns Path object based on given VFS path. File may or may not exist, no exceptions thrown.
     * @param virtualPath relative or absolute VFS path.
     * @return Path object, representing given file.
     */
    public Path toPathNonExistent(String virtualPath) {
        return VFSPath.resolveNonExistent(currentVFSPath, virtualPath).getPathObject();
    }

    /**
     * FTP CPUD command, changes current VFS path.
     * @return VFS upper level directory.
     * @throws NoSuchFileException thrown if currentVFSPath equals to user home directory.
     */
    public String cdupCommand() throws NoSuchFileException {
        currentVFSPath = VFSPath.getParent(currentVFSPath);
        return currentVFSPath.getVirtualPath();
    }

    /**
     * FTP CWD command, changes current VFS path.
     * @param sPath virtual absolute or relative VFS path to navigate to, changes current directory.
     * @return changed VFS path.
     * @throws NoSuchFileException thrown if given path does not exist.
     */
    public String cwdCommand(String sPath) throws NoSuchFileException {
        VFSPath p = VFSPath.resolve(currentVFSPath, sPath);
        if (p.isDirectory()) {
            currentVFSPath = p;
            return currentVFSPath.getVirtualPath(); 
        } else {
            throw new NoSuchFileException("Not a directory. ");
        }
    }

    /**
     * FTP DELE command. Deletes file.
     * @param sPath relative or absolute VFS path.
     * @throws IOException if given file does not exist, or if directory specified instead of file.
     */
    public void deleCommand(String sPath) throws IOException {
        VFSPath p = VFSPath.resolve(currentVFSPath, sPath);
        if (!Files.isDirectory(p.getPathObject())) {
            Files.delete(p.getPathObject()); 
        } else {
            throw new NoSuchFileException("Can not delete directory, use RMD instead. ");
        }
    }

    /**
     * FTP MDTM command.
     * @param sPath absolute or relative path of file to get last modification info.
     * @return last modification info in FTP format.
     * @throws IOException thrown if given file is not retriviable.
     */
    public String mdtmCommand(String sPath) throws IOException {
        VFSPath p = VFSPath.resolve(currentVFSPath, sPath);
        if (!Files.isDirectory(p.getPathObject())) {
            FileTime fTime = IOProcessing.getBasicAttributes(p.getPathObject()).lastModifiedTime();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuuMMddHHmmss");
            String lm = toValidTimeString(fTime, formatter);
            return lm;
        } else {
            throw new NoSuchFileException("not retriviable");
        }
    }

    /**
     * FTP MKD command. Creates a directory with given name.
     * @param sPath absolute or relative VFS path of future directory.
     * @return VFS path of newly created directory.
     * @throws IOException thrown if path already exist.
     */
    public String mkdCommand(String sPath) throws IOException  {
        Path p = toPathNonExistent(sPath);
        return VFSPath.toVirtualPath(currentVFSPath.getUserPathObject(), Files.createDirectories(p));
    }

    /**
     * FTP RMD command. Deletes a directory and all it's contents.
     * @param sPath absolute or relative VFS path to delete directory.
     * @throws IOException if given path does not exist.
     */
    public void rmdCommand(String sPath) throws IOException {
        VFSPath p = VFSPath.resolve(currentVFSPath, sPath);
        if (p.isDirectory()) {
             Files.walk(p.getPathObject()).sorted((p1, p2) -> p2.getNameCount() - p1.getNameCount()
             ).forEach((p1) ->  p1.toFile().delete());
        } else {
            throw new NoSuchFileException("Not a directory, use DELE instead. ");
        }
    }

    /**
     * FTP RNTO command. Renames file or directory. Moves file from place to another.
     * @param rnfr absolute or relative VFS path of file or directory to rename.
     * @param rnto absolute or relative VFS path of file or directory of destination directory.
     * @throws IOException thrown if rnfr does not exist or rnto already exist.
     */
    public void rntoCommand(String rnfr, String rnto) throws IOException {
        Path source = toPath(rnfr);
        Path target = toPathNonExistent(rnto);
        if (!target.getFileName().toString().contains(".")) {
            Files.createDirectories(target);
        }
        if (Files.isDirectory(source) && Files.isDirectory(target)) {
            Files.walk(source).sorted((p1, p2) -> p2.getNameCount() - p1.getNameCount())
                    .forEach((p1) -> {
                try {
                    Path t = target.resolve(source.relativize(p1));
                    Files.createDirectories(t.getParent());
                    if (!(Files.isDirectory(p1) && Files.exists(t))) {
                        Files.move(p1, t, REPLACE_EXISTING);
                    }
                    Files.deleteIfExists(p1);
                } catch (IOException e) {}
            });

        } else if (!Files.isDirectory(source) && Files.isDirectory(target)) {
            Files.createDirectories(target.getParent());
            Files.move(source, target.resolve(source.getFileName()));
        } else if (!Files.isDirectory(source) && !Files.isDirectory(target)) {
            Files.createDirectories(target.getParent());
            Files.move(source, target);
        }
    }

    /**
     * FTP PWD command. Return a current directory.
     * @return a VFS current directory.
     */
    public String pwdCommand() {
        return currentVFSPath.getVirtualPath();
    }

    /**
     * FTP SIZE command. Return file size of given file in bytes.
     * @param sPath relative or absolute VFS path of file to get size.
     * @return file size in bytes.
     * @throws IOException thrown if given file does not exist.
     */
    public String sizeCommand(String sPath) throws IOException  {
        Path path = VFSPath.resolve(currentVFSPath, sPath).getPathObject();
        return Long.toString(IOProcessing.getBasicAttributes(path).size());
    }

    /**
     * FTP LIST command. List files in directory.
     * @param pathString absolute or relative VFS path of directory to list.
     * @return formatted files info.
     * @throws IOException thrown if given path does not exist.
     */
    public String listCommand(String pathString) throws IOException {
        return processFilesInDirectory(pathString, (p) -> getFullFileInfo(p));
    }

    /**
     * FTP NLST command. Similar to LIST command, shorter format.
     * @param pathString absolute or relative VFS path of directory to list.
     * @return formatted files info.
     * @throws IOException thrown if given path does not exist.
     */
    public String nlstCommand(String pathString) throws IOException {
        return processFilesInDirectory(pathString, (p) -> {
            String result = "";
            try {
                result = VFSPath.getByRealPath(currentVFSPath.getUserPathObject(), p.toString()).getVirtualPath();
            } catch (IOException e) {  }
            return result;
        });
    }

    /**
     * Processes files in given directory.
     * @param pathString absolute or relative VFS path of directory.
     * @param mapper function to process each file.
     * @return processed info.
     * @throws IOException thrown if given path does not exist.
     */
    protected String processFilesInDirectory(String pathString, Function<Path, String> mapper) throws IOException{
        String result = "";
        VFSPath path;
        if (pathString.isEmpty()) {
            path = currentVFSPath;
        } else {
            path = VFSPath.resolve(currentVFSPath, pathString);
        }
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(path.getPathObject())) {
            for (Path entry: stream) {
                result = result.concat(mapper.apply(entry).concat(IOProcessing.LINESEPARATOR));
            }
        }
        return result;
    }
    private String getFullFileInfo(Path path) {
        String perms  = Files.isDirectory(path) ? "d" : "-";
        String fn;
        String lm;
        long len;
        String ow;
        String gr;
        DateTimeFormatter df = DateTimeFormatter.ofPattern("MMM dd uuuu", Locale.UK);
        try {
            PosixFileAttributes posixAttrs = Files.readAttributes(path,  PosixFileAttributes.class, LinkOption.NOFOLLOW_LINKS);
            lm = toValidTimeString(posixAttrs.lastModifiedTime(), df);
            ow = posixAttrs.owner().getName();
            gr = posixAttrs.group().getName();
            fn = path.getFileName().toString();
            len = posixAttrs.size();
            Set<PosixFilePermission> posixPermissions = posixAttrs.permissions();
            perms = perms.concat(posixPermissions.contains(PosixFilePermission.OWNER_READ) ? "r" : "-");
            perms = perms.concat(posixPermissions.contains(PosixFilePermission.OWNER_WRITE) ? "w" : "-");
            perms = perms.concat(posixPermissions.contains(PosixFilePermission.OWNER_EXECUTE) ? "x" : "-");
            perms = perms.concat(posixPermissions.contains(PosixFilePermission.GROUP_READ) ? "r" : "-");
            perms = perms.concat(posixPermissions.contains(PosixFilePermission.GROUP_WRITE) ? "w" : "-");
            perms = perms.concat(posixPermissions.contains(PosixFilePermission.GROUP_EXECUTE) ? "x" : "-");
            perms = perms.concat(posixPermissions.contains(PosixFilePermission.OTHERS_READ) ? "r" : "-");
            perms = perms.concat(posixPermissions.contains(PosixFilePermission.OTHERS_WRITE) ? "w" : "-");
            perms = perms.concat(posixPermissions.contains(PosixFilePermission.OTHERS_EXECUTE) ? "x" : "-");
            return String.format("%s %-10s %-10s %d %s %s",  perms,  ow,  gr, len, lm,  fn); 
            
        } catch (UnsupportedOperationException | IOException e) {  }
        
        try {
            AclFileAttributeView aclAttrs = Files.getFileAttributeView(path, AclFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
            UserPrincipal owner = aclAttrs.getOwner();
            UserPrincipal group = owner;
            String ownerR = "-";
            String ownerW = "-";
            String ownerX = "-";
            Stream<AclEntry> entries = aclAttrs.getAcl().stream().filter((entry) -> entry.principal().equals(owner));
            for (AclEntry entry: entries.toArray(AclEntry[]::new)) {
                Set<AclEntryPermission> permissions = entry.permissions();
                if (entry.type() == AclEntryType.ALLOW) {
                    ownerR = permissions.contains(AclEntryPermission.READ_DATA)? "r" : "-";
                    ownerW = permissions.contains(AclEntryPermission.WRITE_DATA)? "w" : "-";
                    ownerX = permissions.contains(AclEntryPermission.EXECUTE)? "x" : "-";
                }
            }
            perms = perms.concat(ownerR.concat(ownerW.concat(ownerX)));
            perms = perms.concat(perms.substring(1)).concat(perms.substring(1));
            BasicFileAttributes attrs = Files.readAttributes(path, BasicFileAttributes.class);
            fn = path.getFileName().toString();
            len = attrs.size();
            ow = owner.getName();
            gr = group.getName();
            lm = toValidTimeString(attrs.lastModifiedTime(), df);
            return String.format("%s %-10s %-10s %d %s %s",  perms,  ow,  gr, len, lm,  fn); 
        } catch (IOException e) {
            System.err.println(e);
        }
        return IOProcessing.EMPTY;
    }
    private String toValidTimeString(FileTime fTime, DateTimeFormatter formatter) {
        String time = fTime.toString();
        time = time.substring(0, time.indexOf("Z"));
        LocalDateTime d = 
            LocalDateTime.parse(time, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        return d.format(formatter);
    }
}
