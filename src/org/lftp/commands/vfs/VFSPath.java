package org.lftp.commands.vfs;

import java.io.*;
import java.nio.file.*;
import java.util.Objects;

class VFSPath implements Comparable<VFSPath> {
    protected static Path HOMEPATH;
    private static final String ERRORMESSAGE = "File not exists. ";
    private final Path userPathObject;
    private final String virtualPath;
    private final Path pathObject;
    
    // private constructor
    private VFSPath(Path userPath, Path path) {
        userPathObject = userPath.normalize();
        if (!Files.exists(userPath)) {
            try {
                Files.createDirectory(userPath);
            } catch (IOException e) { }
        }
        pathObject = path.normalize();
        virtualPath = VFSPath.toVirtualPath(userPathObject, pathObject);
    }
    
    // factory methods
    public static VFSPath newInstance(String username) {
        Path p = HOMEPATH.resolve(username);
        return new VFSPath(p, p);
    }
//    public static VFSPath newInstance(Path path) {
//        return new VFSPath(path);
//    }
    public static VFSPath getByRealPath(Path userPath, String realAbsolutePath) throws NoSuchFileException {
        Path path = userPath.resolve(realAbsolutePath).normalize();
        if (isValidPath(userPath, path)) {
            return new VFSPath(userPath, path);
        } else {
            throw new NoSuchFileException(ERRORMESSAGE);
        }
    }
    public static VFSPath getByVirtualPath(Path userPath, String virtualAbsolutePath) throws NoSuchFileException {
        Path path = getPath(userPath, userPath, virtualAbsolutePath);
        if (isValidPath(userPath, path)) {
            return new VFSPath(userPath, path);
        } else {
            throw new NoSuchFileException(ERRORMESSAGE);
        }
    }
    public static VFSPath getParent(VFSPath vfsPathToGetParent) throws NoSuchFileException {
        return VFSPath.resolve(vfsPathToGetParent, "..");
    }
    public static VFSPath resolve(VFSPath vfsPath, String virtualPath) throws NoSuchFileException {
        VFSPath p = resolveNonExistent(vfsPath, virtualPath);
        if (isValidPath(p.userPathObject, p.pathObject)) {
            return p;
        } else {
            throw new NoSuchFileException(ERRORMESSAGE);
        }
    }
    public static VFSPath resolveNonExistent(VFSPath vfsPath, String virtualPath) {
        Path p;
        switch (virtualPath) {
            case "..":
                p = vfsPath.getPathObject().getParent().normalize();
                break;
            case "":
                p = vfsPath.getPathObject();
                break;
            default:
                p = getPath(vfsPath.userPathObject, vfsPath.getPathObject(), virtualPath);
                break;
        }
        return new VFSPath(vfsPath.userPathObject, p);
    }
    
    // overriden methods
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object instanceof VFSPath) {
            VFSPath p = (VFSPath) object;
            return pathObject.equals(p.pathObject);
        } else {
            return false;
        }
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.virtualPath);
        return hash;
    }
    @Override
    public int compareTo(VFSPath p) {
        return HOMEPATH.compareTo(p.pathObject);
    }
    // instance methods
    public String getRealPath() {
        return pathObject.toString();
    }
    public boolean isDirectory() {
        return Files.isDirectory(pathObject);
    }
    public String getVirtualPath() {
        return virtualPath;
    }
    public Path getPathObject() {
        return pathObject;
    }
    protected Path getUserPathObject() {
        return userPathObject;
    }
    public File toFile() {
        return pathObject.toFile();
    }
    @Override
    public String toString() {
        return getVirtualPath();
    }
    
    // helper methods
   protected static String resolveVirtualPath(VFSPath vfsPath, String virtualPath) {
        if (virtualPath.startsWith("/")) {
            return virtualPath;
        } else {
            return vfsPath.getVirtualPath().concat("/").concat(virtualPath);
        }
   }
    protected static Path getPath(Path userPath, Path path, String virtualPath) {
        String relativeRealPath = virtualPath.replace("/", "\\");
        if (virtualPath.startsWith("/")) {
            return userPath.resolve(relativeRealPath.substring(1));
        } else {
            return path.resolve(relativeRealPath);
        }
    }
    protected static String toVirtualPath(Path userPath, Path pathObject) {
        Path relative = userPath.relativize(pathObject).normalize();
        return "/".concat(relative.toString().replace("\\", "/"));
    }
    private static boolean isValidPath(Path userPath, Path path) {
        return userPath.compareTo(path) <= 0 && Files.exists(path);
    }
}
