package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import org.lftp.ioprocessing.IOProcessing;
import java.io.*;
import java.util.logging.Level;
import static org.lftp.commands.Command.LOGGER;

class RNTOCommand extends LoginRequiredCommand {
    protected RNTOCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }

    @Override
    protected boolean prepare() {
        if (!super.prepare()) {
            return false;
        }
        if (connection.renameFrom.isEmpty()) {
            connection.writeAnswer(503, "commandWrongSequence");
            return false;
        }
        return true;
    }

    @Override
    protected void performAction() {
        if (!connection.workingDirectory.fileExists(argument)) {
            try {
                connection.workingDirectory.rntoCommand(connection.renameFrom, argument);
                connection.writeAnswer(250, "rntoSuccess");
            } catch (IOException e) {
                LOGGER.log(Level.INFO, String.format("IO error in %s %s", RNTOCommand.class.getSimpleName(), connection.toString()), e);
                connection.writeAnswer(450, "ioError");
            }
        } else {
            connection.writeAnswer(550, "fileAlreadyExists");
        }
        connection.renameFrom = IOProcessing.EMPTY;
    }
}
