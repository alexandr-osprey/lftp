package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import org.lftp.ioprocessing.IOProcessing;
import java.io.*;
import java.nio.file.*;
import java.util.logging.Level;
import static org.lftp.commands.Command.LOGGER;

class LISTCommand extends DataRequiredCommand {
    protected LISTCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        try {
            String list = connection.workingDirectory.listCommand(argument);
            IOProcessing.writeText(connection.getDataChannel(), connection.charset,  connection.BYTEBUFFER, list);
            connection.writeAnswer(226, "listSuccess");
        } catch (NoSuchFileException e) {
            connection.writeAnswer(550, "directoryNotFound", argument);
        } catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("IO error in %s %s", LISTCommand.class.getSimpleName(), connection.toString()), e);
            connection.writeAnswer(450, "dataConnectionError");
        } finally {
            connection.closeDataChannel();
        }
    }
}
