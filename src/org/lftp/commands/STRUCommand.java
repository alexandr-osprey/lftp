package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import org.lftp.connection.FTPConnection.STRU;

class STRUCommand extends LoginRequiredCommand {
    protected STRUCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        switch (argument.toUpperCase()) {
            case "F":
               connection.dataStru = STRU.FILE;
               connection.writeAnswer(200, "struSuccess");
            default:
               connection.writeAnswer(504, "parameterUnknown");
       } 
    }
}
