package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.nio.file.*;

class CDUPCommand extends LoginRequiredCommand {
    protected CDUPCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        try {
            connection.writeAnswer(200, "cdupSuccess", connection.workingDirectory.cdupCommand());
        } catch (NoSuchFileException e) {
            connection.writeAnswer(550, "directoryNotFound");
        }
    }
}
