package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import java.io.IOException;
import java.nio.file.*;
import java.util.logging.Level;

class DELECommand extends LoginRequiredCommand {
    protected DELECommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        try {
            connection.workingDirectory.deleCommand(argument);
            connection.writeAnswer(250, "deleSuccess");
        } catch (NoSuchFileException e) {
            connection.writeAnswer(501, "fileNotFound");
        } catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("IO error in %s %s", DELECommand.class.getSimpleName(), connection.toString()), e);
            connection.writeAnswer(450, "ioError");
        }
    }
}
