package org.lftp.commands;

import org.lftp.connection.FTPConnection;

class ACCTCommand extends Command {
    protected ACCTCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    
    @Override
    protected void performAction() {
        connection.writeAnswer(202, "commandNotImplementedSuperfluous");
    }
}
