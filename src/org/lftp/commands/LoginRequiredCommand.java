package org.lftp.commands;

import org.lftp.connection.FTPConnection;

abstract class LoginRequiredCommand extends Command {
    protected LoginRequiredCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected boolean prepare() {
        return super.prepare() && loggedIn();
    }
    protected boolean loggedIn() {
        if (connection.getUser() == null) {
            connection.writeAnswer(530, "userNotLogged");
            return false;
        }
        return true;
    }
}
