package org.lftp.commands;

import org.lftp.connection.FTPConnection;

class NOOPCommand extends Command {
    protected NOOPCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        connection.writeAnswer(200, "systemOk");
    }
}
