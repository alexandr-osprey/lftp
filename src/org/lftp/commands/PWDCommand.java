package org.lftp.commands;

import org.lftp.connection.FTPConnection;

class PWDCommand extends LoginRequiredCommand {
    protected PWDCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        connection.writeAnswer(257, "empty", connection.workingDirectory.pwdCommand());
    }
}
