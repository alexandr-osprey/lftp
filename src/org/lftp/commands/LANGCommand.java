package org.lftp.commands;

import org.lftp.connection.FTPConnection;

class LANGCommand extends Command {
    protected LANGCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected void performAction() {
        String language;
        String country;
        if (argument.contains("-")) {
            language = argument.substring(0, argument.indexOf("-")).toLowerCase();
        } else {
            language = argument.toLowerCase();
        }
        switch (language) {
            case "en" :
                country = "US";
                break;
            case "ru" :
                country = "RU";
                break;
            default:
                connection.writeAnswer(504, "parameterUnknown");
                return;
        }
        connection.setLocale(language, country);
        connection.writeAnswer(200, "langSuccess");
    }
}
