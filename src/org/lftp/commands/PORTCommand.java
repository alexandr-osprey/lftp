package org.lftp.commands;

import org.lftp.connection.FTPConnection;
import static org.lftp.connection.LFTP.getLocal4;
import java.io.*;
import java.net.*;
import java.nio.channels.*;
import java.util.logging.Level;
import static org.lftp.commands.Command.LOGGER;

class PORTCommand extends LoginRequiredCommand {
    private String[] address;
    protected PORTCommand(FTPConnection connection, String argument) {
        super(connection, argument);
    }
    @Override
    protected boolean prepare() {
        if (!super.prepare()) {
            return false;
        }
        if (connection.EPSVALL) {
            connection.writeAnswer(503, "epsvAll");
            return false;
        }
        address = argument.substring(0, argument.length()).split(",");
        if (!(address.length == 6 || address.length == 5)) {
            connection.writeAnswer(501, "syntaxError");
            return false;
        }
        return true;
    }
    @Override
    protected void performAction() {
        String ipv4 = "";
        for (int i = 0; i < 3; i++) {
            ipv4 = ipv4.concat(address[i]).concat(".");
        }
        ipv4 = ipv4.concat(address[3]);
        String p = "";
        for (int i = 4; i < address.length; i++) {
            p = p.concat(address[i]);
        }
        int port = Integer.parseInt(p);
        try {
            InetAddress remoteAddress = Inet4Address.getByName(ipv4);
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(remoteAddress, port));
            SocketChannel channel = SocketChannel.open();
            channel.bind(new InetSocketAddress(getLocal4(), 0));
            channel.connect(new InetSocketAddress(remoteAddress, port));
            connection.setDataChannel(channel);
            connection.writeAnswer(200, "dataConnectionOpen");
        } catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("IO error in %s %s", PORTCommand.class.getSimpleName(), connection.toString()), e);
            connection.writeAnswer(425, "dataConnectionError");
        }
    }
    
}
